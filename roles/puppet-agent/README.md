# PUPPET-AGENT

Role to install and configure the puppet agent on the VMs.

## Tasks

### [prerequisites-archlinux.yml](tasks/prerequisites-archlinux.yml)

Install following prerequisites packages for ArchLinux:

- facter

### [prerequisites-ubuntu.yml](tasks/prerequisites-ubuntu.yml)

Install following prerequisites packages for Ubuntu:

- facter

### [prerequisites-archlinux.yml](tasks/prerequisites-archlinux.yml)

Install following prerequisites packages for ArchLinux:

- facter

### [install-archlinux.yml](tasks/install-archlinux.yml)

Install following packages for ArchLinux:

- puppet

### [install-ubuntu.yml](tasks/install-ubuntu.yml)

Add Puppet repository and install following packages for Ubuntu:

- puppet

### [config.yml](tasks/config.yml)

Add extra path, update puppet conf, remove old certificate (if necessary) and start and enable puppet agent.

### [cert-request.yml](tasks/cert-request.yml)

Request puppet certification to the puppet master.

## Variables

- _hostname_: Current host's name (act as puppet slave);
- _puppetserver_name_: Puppet server's name;
- _environment_: Environment of the VM (common, test, prod).

## TODO

- [ ] Check the archlinux install
