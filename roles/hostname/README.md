# HOSTNMAE

This role sets the hostname. 

## Tasks

Here is a list of all tasks and a description of each of them.

### [task1.yml](tasks/task1.yml)

This task can be executed on any linux operating system without distinction (for instance a configuration task).

### [task2-ubuntu.yml](tasks/task2-ubuntu.yml)

This task can only be executed on an Ubuntu OS Linux.

## Templates

A list of all templates.

## Variables

- _vars1_: variable 1

## TODO

- [ ] todo 1
- [ ] todo 2

Version: 20201204
