# OS-UPDATE
Role to update any OS.

## Tasks

### update-archlinux
Task to update an Arch Linux based VM.

### update-centos
Task to update a CentOS VM.

### update-ubuntu
Task to update an Ubuntu VM.
