# K3SMASTER 

This role configures a k3s master node.

## Tasks

Here is a list of all tasks and a description of each of them.

### [config.yml](tasks/config.yml)

This task configures the Raspberry Pi 4. 

### [install.yml](tasks/install.yml)

This task install the K3S master.

## Variables

Version: 20201204
