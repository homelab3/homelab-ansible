# REBOOT

Role to reboot a VM.

## Tasks

### reboot
Reboot the VM and wait until it restarted.