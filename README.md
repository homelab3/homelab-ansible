# Homelab-Ansible

Ansible for the proxmox homelab.

## Table of Content

- [Homelab-Ansible](#homelab-ansible)
  - [Table of Content](#table-of-content)
  - [Introduction and Goals](#introduction-and-goals)
  - [Installation](#installation)
    - [Host](#host)
    - [Remote host](#remote-host)
  - [Architecture](#architecture)
  - [Projects](#projects)
    - [Common](#common)
    - [Test](#test)
    - [Prod](#prod)
  - [Ansible commands](#ansible-commands)
  - [Sources](#sources)
  - [TODO](#todo)

## Introduction and Goals

Ansible is an automation tool. This repository contains the the Ansible configuration to configure VMs on the Proxmox homelab.

## Installation

### Host

```zsh
sudo apt install ansible
```

#### Configuration

The ansible configuration file is located under `/etc/ansible/ansible.cfg`.
The following variables need to be set:

```
[defaults]
inventory = PATH_TO_HOSTS_FILE
roles_path = PATH_TO_ROLES_FOLDER
private_key_file = PATH_TO_SSH_KEY
```

### Remote host

There is nothing to do on the remote host as Ansible connects to it via SSH. The remote host must be added to the [homelab-hosts](https://gitlab.com/homelab3/homelab-ansible/-/blob/master/homelab-hosts).

## Architecture

## Projects

### Lab

Empty

### Test

Empty

### Prod

- p2tools1a

## Ansible Commands

### Ping all hosts

```zsh
ansible all -m ping -i hosts
```

### Run Playbook

```zsh
ansible-playbook PATH_TO_PLAYBOOK -i hosts
```

Example:

```zsh
ansible-playbook playbook/p2tools1a.yml -i hosts
```

### Creatng new role

To keep consitancy across all roles, a role template can be found under roles/01_template.

## Sources

## TODO

- [ ] 20201007: review all README
- [ ] 20201019: add links to the tasks in the README (see docker task)
- [ ] 20210204: update this README
